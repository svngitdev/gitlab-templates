# gitlab templates

Collection of templates to be used in CI/CD of projects

## How to use

You can include templates in your project. Please don't use the master reference to prevent your pipeline to be broken when we do a breaking change. Use tags or commit reference.

## Rules of this repo

- Only hidden jobs, should not be triggered by default, expect for proxy folder
- Scoped, starts with `.igloo:filename:job`
