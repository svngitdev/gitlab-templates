const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  ssr: false,
  target: 'static',

  head: {
    title: 'Igloo templates docs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ]
  },

  buildModules: [
    '@nuxt/typescript-build'
  ],

  modules: [
    '@nuxtjs/bulma',
    '@nuxtjs/pwa'
  ],

  router: {
    base
  },

  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    }
  }
}
